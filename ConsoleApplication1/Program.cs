﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace ConsoleApplication1
{

    //добавить тесты
    //добавить таймер
    //добавить остановку в брутфорсе
    class Program
    {
        public static bool stopA = false;
        public class Gen
        {
            Int64 coefA = 0;
            Int64 coefB = 0;
            Int64 coefM = 0;

            public Gen(int min, int max){
                Random rnd = new Random();
                coefA = rnd.Next(min, max+1);
                coefB = rnd.Next(min, max + 1);
                coefM = rnd.Next(min, max + 1);
                Console.WriteLine(coefA.ToString() + " A  B " + coefB.ToString() + " " + coefM.ToString() + " M");
            }
            /*TODO: функция с параметрами (начальная популяция количество генов
             * вероятность кроссовера (инт 1-100)
             * вероятность мутанта (инт 1-100)
             * вероятность инверсии (инт 1-100)
             * максимально эпох (1-max)
             */
            //преобразование в коды грея и обратно в отдельном файле например
            //Гены тоже сделать в отдельный файл
            //функция мутации гена
            public Int64[] mutant(Gen gen)
            {
                return new Int64[] { 1, 2, 3 };
            }
            //Функция кроссовера гена
            public Int64[] crossover(Gen gen)
            {
                return new Int64[] { 1, 2, 3 };
            }
            //функция инверсии гена
            public Int64[] inverse(Gen gen)
            {
                return new Int64[] { 1, 2, 3 };
            }

            public float fitnes(Gen gen)
            {
                float output = 3.4F;
                return output;
            }
        }

        static void brute4Threads()
        {
            Thread thread1 = new Thread(() => brut.bruteforce32(521162, 999873, 341526, 609445, 0, 250000));
            Thread thread2 = new Thread(() => brut.bruteforce32(521162, 999873, 341526, 609445, 250000, 500000));
            Thread thread3 = new Thread(() => brut.bruteforce32(521162, 999873, 341526, 609445, 500000, 750000));
            Thread thread4 = new Thread(() => brut.bruteforce32(521162, 999873, 341526, 609445, 750000, 999999));
            thread1.Start();
            thread2.Start();
            thread3.Start();
            thread4.Start();
        }
        public class brut
        {
            //public static bool stopThreadBool = false;
            public static void bruteforce32(Int64 num1, Int64 num2, Int64 num3, Int64 num4, int start, int finish)
            {
                DateTime dold = DateTime.Now;
                Int64 now1 = 0;
                Int64 now2 = 0;
                Int64 now3 = 0;

                // a ot 0 do 1m, a poisk C - threads po 250k
                for (Int64 a = 0; a < 999999; a++)
                    for (Int64 c = start; c < finish; c++)
                    {
                        now1 = (a * num1 + c) % 1000000;
                        if (stopA == true)
                        { 
                            return; 
                        }
                        if (now1 == num2)
                        {
                            now2 = (a * num2 + c) % 1000000;
                            if (now2 == num3)
                            {
                                now3 = (a * num3 + c) % 1000000;
                                if (now3 == num4)
                                {
                                    Console.WriteLine(a.ToString() + " <--- a, c----> " + c.ToString());
                                    TimeSpan sp = DateTime.Now - dold;
                                    stopA = true;
                                    Console.WriteLine("Время поиска полным перебором: " + sp.ToString()+ " ");
                                    return;
                                }
                            }
                        }
                    }

            }
        }
        public static Int64 FindModulus(Int64 value1, Int64 value2)
        {
            //Mr. Euclid наибольший общий делитель
            while (value2 != 0)
                value2 = value1 % (value1 = value2);
            return value1;
        }
        static void Main(string[] args)
        {
            int k=50;
            string f = @"C:\Users\lstk\Desktop\5kurs\Lesko\50\Task2\1";
            Console.WriteLine("File number " + k.ToString());

            List<string> lines = new List<string>();
            List<Int64> intes = new List<Int64>();

            using (StreamReader r = new StreamReader(f))
            {
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    intes.Add(Convert.ToInt64(line));
                }
            }

            Int64[] Ts = new Int64[150];
            Int64[] Ss = new Int64[145];
            
            //choosing variables for matrix
            Int64 a1 = 0;
            Int64 a2 = 0;
            Int64 a3 = 0;
            Int64 a4 = 0;

            List<Int64> deters = new List<Int64>();
            
            for (int i = 1; i < 1000; i++ )
            {
                a1 = intes[i] - intes[0]; //n1=last-20 n2=last-19
                a2 = intes[i+1] - intes[1];
                a3 = intes[i+5] - intes[0];
                a4 = intes[i+6] - intes[1];
                Int64[,] matrix = { { a1, a2 }, { a3, a4 } };
                Int64 determinant = matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
                deters.Add(determinant);
            }



            Int64 modul = 0;
            Console.WriteLine("Size: " + deters.Count.ToString());
            modul = FindModulus(deters[0], deters[1]);

                    for (int i = 1; i < deters.Count; i++)
                    {
                        modul = FindModulus(modul, deters[i]);
                    }
            Console.WriteLine(" modul = " + Math.Abs(modul));
            Console.WriteLine("   ");

            Console.WriteLine("Start bruteforce....");
            //OPEN file and get 1st 5 numbers

            //start bruteforce in 4 threads from 0 to 1m
            
                            brute4Threads();                
            

            Gen first = new Gen(0, 1000000);

            Console.ReadLine();
            Console.WriteLine("VSE !!!!!");
            Console.ReadLine();

        }

    }
}
